package com.petrovsky.test.webapp.controller;

import com.petrovsky.test.model.Employee;
import com.petrovsky.test.repository.jdbc.EmployeeDao;
import com.petrovsky.test.utils.JsonUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.sql.SQLException;

@Controller
@RequestMapping("api/employee")
public class EmployeeController {

    private static final String UNDEFINED_CLIENT_DATA = "undefined";

    @Autowired
    private EmployeeDao employeeDao;

    private Boolean employeeValidation (Employee employee) {
        return employee != null &&
                employee.name != null &&
                employee.department != null &&
                employee.department.id != null &&
                employee.department.name != null;
    }

    @RequestMapping(value = "/add", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public String saveEmployee(@RequestBody String json) throws SQLException {
        if (!json.isEmpty() && !json.equals(UNDEFINED_CLIENT_DATA)) {
            Employee employee = JsonUtils.fromJson(Employee.class, json);
            if (employeeValidation(employee)) employeeDao.save(employee);
            return "OK";
        } else {
            return "Not correct sent data";
        }
    }

    @RequestMapping(value = "/delete", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public String deleteEmployee(@RequestBody String json) throws SQLException {
        if (!json.isEmpty() && !json.equals(UNDEFINED_CLIENT_DATA)) {
            employeeDao.delete(((Employee)JsonUtils.fromJson(Employee.class, json)).id);
            return "OK";
        } else {
            return "Not correct sent data";
        }
    }

    @RequestMapping(value = "/get/all/count", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public String getAllEmployeesCount() throws SQLException {
        return employeeDao.getAllEmployeesCount().toString();
    }

    @RequestMapping(value = "/get/all/{currentPage}-{pageSize}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public String getAllEmployees(@PathVariable(value = "currentPage") String currentPage,
                                  @PathVariable(value = "pageSize") String pageSize) throws SQLException {
        if(NumberUtils.isNumber(currentPage) && NumberUtils.isNumber(pageSize)) {
            return JsonUtils.getJson(employeeDao.getAllEmployees(Integer.parseInt(currentPage), Integer.parseInt(pageSize)));
        } else {
            return null;
        }
    }

    @RequestMapping(value = "/get/by/name/{name}/count", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public String searchEmployeesByNameCount(@PathVariable(value = "name") String name) throws SQLException {
        if(!name.isEmpty()) {
                return employeeDao.getEmployeesByLikeNameCount(name).toString();
        } else {
            return null;
        }
    }

    @RequestMapping(value = "/get/by/name/{name}/{currentPage}-{pageSize}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public String searchEmployeesByName(@PathVariable(value = "name") String name,
                                        @PathVariable(value = "currentPage") String currentPage,
                                        @PathVariable(value = "pageSize") String pageSize) throws SQLException {
        if(!name.isEmpty()) {
            if(NumberUtils.isNumber(currentPage) && NumberUtils.isNumber(pageSize)) {
                return JsonUtils.getJson(employeeDao.searchEmployeesByName(name, Integer.parseInt(currentPage), Integer.parseInt(pageSize)));
            } else {
                return null;
            }
        } else {
            return null;
        }
    }
}
