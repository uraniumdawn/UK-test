package com.petrovsky.test.webapp.controller;

import com.petrovsky.test.repository.jdbc.DepartmentDao;
import com.petrovsky.test.utils.JsonUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.sql.SQLException;

@Controller
@RequestMapping("api/department")
public class DepartmentController {

    @Autowired
    private DepartmentDao departmentDao;

    @RequestMapping(value = "/get/all", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public String getAllDepartment() throws SQLException {
        return JsonUtils.getJson(departmentDao.getAllDepartments());
    }
}
