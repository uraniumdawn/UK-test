package com.petrovsky.test.model;

import com.google.gson.annotations.SerializedName;

public class Employee {
    public static final String ID_JSON = "id";
    public static final String NAME_JSON = "nm";
    public static final String ACTIVE_JSON = "acv";
    public static final String DEPARTMENT_JSON = "dp";

    @SerializedName(ID_JSON)
    public Long id;

    @SerializedName(NAME_JSON)
    public String name;

    @SerializedName(ACTIVE_JSON)
    public Boolean active;

    @SerializedName(DEPARTMENT_JSON)
    public Department department;

}
