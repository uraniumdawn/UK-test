package com.petrovsky.test.model;

import com.google.gson.annotations.SerializedName;

public class Department {
    public static final String ID_JSON = "id";
    public static final String NAME_JSON = "nm";

    @SerializedName(ID_JSON)
    public Long id;

    @SerializedName(NAME_JSON)
    public String name;
}
