package com.petrovsky.test.repository.jdbc;

import com.petrovsky.test.model.Department;
import com.petrovsky.test.model.Employee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.sql.*;
import java.util.LinkedList;
import java.util.List;

@Repository
public class EmployeeDao {

    private static final String SAVE_EMPLOYEE = "INSERT INTO tblEmployees (empName, empActive, emp_dpID) VALUES (?, ?, ?)";
    private static final String UPDATE_EMPLOYEE = "UPDATE tblEmployees SET empName = ?, empActive = ?, emp_dpID = ? WHERE empID = ?";
    private static final String DELETE_EMPLOYEE = "DELETE FROM tblEmployees WHERE empID = ?";

    private static final String SELECT_ALL_EMPLOYEE_PNG_COUNT = "EXECUTE SelectAllEmployeesPngCount";
    private static final String SELECT_ALL_EMPLOYEE_PNG = "EXECUTE SelectAllEmployeesPng ?, ?";

    private static final String SELECT_BY_LIKE_NAME_EMPLOYEE_PNG_COUNT = "EXECUTE SelectByLikeNameEmployeesPngCount ?";
    private static final String SELECT_BY_LIKE_NAME_EMPLOYEE_PNG = "EXECUTE SelectByLikeNameEmployeesPng ?, ?, ?";

    @Autowired
    private DataSource dataSource;

    private List<Employee> getEmployeeFromResultSet (ResultSet rs) throws SQLException {
        List<Employee> employees = new LinkedList<>();

        while(rs.next()){
            Employee employee = new Employee();
            Department department = new Department();
            employee.id = rs.getLong("empID");
            employee.name = rs.getString("empName");
            employee.active = rs.getBoolean("empActive");
            department.id = rs.getLong("emp_dpID");
            department.name = rs.getString("dpName");
            employee.department = department;
            employees.add(employee);
        }
        return employees;
    }

    public Employee save (Employee employee) throws SQLException {

        if(employee != null) {
            Connection connection = dataSource.getConnection();
            PreparedStatement preparedStatement;
            if(employee.id == null) {
                preparedStatement = connection.prepareStatement(SAVE_EMPLOYEE);
                preparedStatement.setString(1, employee.name);
                preparedStatement.setBoolean(2, employee.active);
                preparedStatement.setLong(3, employee.department.id);
                preparedStatement.executeUpdate();
            } else {
                preparedStatement = connection.prepareStatement(UPDATE_EMPLOYEE);
                preparedStatement.setString(1, employee.name);
                preparedStatement.setBoolean(2, employee.active);
                preparedStatement.setLong(3, employee.department.id);
                preparedStatement.setLong(4, employee.id);
                preparedStatement.executeUpdate();
            }
            connection.close();
            preparedStatement.close();
        }
        return employee;
    }

    public void delete (Long id) throws SQLException {

        Connection connection = dataSource.getConnection();
        PreparedStatement preparedStatement = connection.prepareStatement(DELETE_EMPLOYEE);
        preparedStatement.setLong(1, id);
        preparedStatement.executeUpdate();
        connection.close();
        preparedStatement.close();
    }

    public Integer getAllEmployeesCount () throws SQLException {

        Connection connection = dataSource.getConnection();
        Statement statement = connection.createStatement();
        ResultSet resultSet = statement.executeQuery(SELECT_ALL_EMPLOYEE_PNG_COUNT);
        resultSet.next();
        int count = resultSet.getInt(1);

        connection.close();
        statement.close();

        return count;
    }

    public List<Employee> getAllEmployees (int currentPage, int pageSize) throws SQLException {

        Connection connection = dataSource.getConnection();
        PreparedStatement preparedStatement = connection.prepareStatement(SELECT_ALL_EMPLOYEE_PNG);
        preparedStatement.setInt(1, currentPage);
        preparedStatement.setInt(2, pageSize);
        ResultSet resultSet = preparedStatement.executeQuery();
        List<Employee> employees = getEmployeeFromResultSet(resultSet);

        connection.close();
        preparedStatement.close();

        return employees;
    }

    public Integer getEmployeesByLikeNameCount (String partOfName) throws SQLException {

        Connection connection = dataSource.getConnection();
        PreparedStatement preparedStatement = connection.prepareStatement(SELECT_BY_LIKE_NAME_EMPLOYEE_PNG_COUNT);
        preparedStatement.setString(1, partOfName + "%");
        ResultSet resultSet = preparedStatement.executeQuery();
        resultSet.next();
        int count = resultSet.getInt(1);

        connection.close();
        preparedStatement.close();

        return count;
    }

    public List<Employee> searchEmployeesByName (String partOfName, int currentPage, int pageSize) throws SQLException {

        Connection connection = dataSource.getConnection();
        PreparedStatement preparedStatement = connection.prepareStatement(SELECT_BY_LIKE_NAME_EMPLOYEE_PNG);
        preparedStatement.setString(1, partOfName + "%");
        preparedStatement.setInt(2, currentPage);
        preparedStatement.setInt(3, pageSize);
        ResultSet resultSet = preparedStatement.executeQuery();
        List<Employee> employees = getEmployeeFromResultSet(resultSet);

        connection.close();
        preparedStatement.close();

        return employees;
    }
}
