package com.petrovsky.test.repository.jdbc;

import com.petrovsky.test.model.Department;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;

@Repository
public class DepartmentDao {
    private static final String SELECT_ALL_DEPARTMENT = "SELECT dpID, dpName FROM tblDepartments";

    @Autowired
    private DataSource dataSource;

    private List<Department> getDepartmentFromResultSet (ResultSet rs) throws SQLException {
        List<Department> departments = new LinkedList<>();

        while(rs.next()){
            Department department = new Department();
            department.id = rs.getLong("dpID");
            department.name = rs.getString("dpName");
            departments.add(department);
        }
        return departments;
    }

    public List<Department> getAllDepartments () throws SQLException {

        Connection connection = dataSource.getConnection();
        Statement statement = connection.createStatement();
        ResultSet resultSet = statement.executeQuery(SELECT_ALL_DEPARTMENT);
        List<Department> departments = getDepartmentFromResultSet(resultSet);

        connection.close();
        statement.close();

        return departments;
    }
}
