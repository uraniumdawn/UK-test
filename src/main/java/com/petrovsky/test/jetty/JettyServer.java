package com.petrovsky.test.jetty;

import com.petrovsky.test.settings.ServerSettings;
import org.eclipse.jetty.server.Handler;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.handler.HandlerList;
import org.eclipse.jetty.server.handler.ResourceHandler;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.springframework.web.filter.CharacterEncodingFilter;
import org.springframework.web.servlet.DispatcherServlet;

import javax.servlet.DispatcherType;
import java.net.URL;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;

public class JettyServer {

    public static final String WEB_APP_ROOT = "webapp";
    public static final String STATIC_RESOURCE_ROOT = "web";
    public static final String[] WELCOME_FILES = {"index.html"};
    public static final String MVC_SERVLET_NAME = "rest";

    private Server server;
    private ServerSettings settings;

    public JettyServer(ServerSettings settings) {
        this.settings = settings;
    }

    public void start() {
        server = new Server(settings.getPort());

        ResourceHandler webResourceHandler = new ResourceHandler();
        webResourceHandler.setDirectoriesListed(true);
        webResourceHandler.setWelcomeFiles(WELCOME_FILES);
        webResourceHandler.setResourceBase(STATIC_RESOURCE_ROOT);

        HandlerList handlers = new HandlerList();
        handlers.setHandlers(new Handler[]{webResourceHandler, getServletHandler()});
        server.setHandler(handlers);

        try {
            server.start();
        } catch (Exception e) {
            System.exit(0);
        }

    }

    private ServletContextHandler getServletHandler() {

        ServletContextHandler context = new ServletContextHandler(ServletContextHandler.SESSIONS);
        context.setClassLoader(Thread.currentThread().getContextClassLoader());

        ServletHolder mvcServletHolder = new ServletHolder(MVC_SERVLET_NAME, new DispatcherServlet());
        mvcServletHolder.setInitParameter("contextConfigLocation", "web-context.xml");
        context.addServlet(mvcServletHolder, "/");
        mvcServletHolder.setAsyncSupported(true);
        mvcServletHolder.setInitOrder(1);

        Map<String, String> parameters = new HashMap<>();
        parameters.put("encoding", "UTF-8");
        parameters.put("forceEncoding", "true");
        context.addFilter(CharacterEncodingFilter.class, "/*", EnumSet.allOf(DispatcherType.class))
                .setInitParameters(parameters);

        context.setResourceBase(getBaseUrl());

        return context;
    }

    public void join() throws InterruptedException {
        server.join();
    }

    public void stop() throws Exception {
        server.stop();
    }

    private String getBaseUrl() {
        URL webInfUrl = JettyServer.class.getClassLoader().getResource(WEB_APP_ROOT);
        if (webInfUrl == null) {
            throw new RuntimeException("Failed to find web application root: " + WEB_APP_ROOT);
        }
        return webInfUrl.toExternalForm();
    }
}
