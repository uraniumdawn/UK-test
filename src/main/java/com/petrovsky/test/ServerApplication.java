package com.petrovsky.test;

import com.petrovsky.test.jetty.JettyServer;
import com.petrovsky.test.settings.ServerSettings;
import com.petrovsky.test.settings.SettingsFactory;

public class ServerApplication {

	public static void main(String args[]) throws Exception {
		ServerSettings settings = SettingsFactory.getInstance();
		try {
			JettyServer server = new JettyServer(settings);
			server.start();
			server.join();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
