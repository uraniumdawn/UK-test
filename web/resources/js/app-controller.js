app.controller('AppController', [
    '$scope',
    'AppService',
    function ($scope, AppService) {
        console.log("Init AppController");

        $scope.employees = [];
        $scope.employeesCount = null;

        $scope.totalPages = [];
        $scope.pageSize = 10;
        $scope.totalPagesInt = 0;
        $scope.pagesPerPage = 10;
        $scope.currentBunch = 1;

        $scope.srchTotalPages = [];
        $scope.srchPageSize = 10;
        $scope.srchTotalPagesInt = 0;
        $scope.srchPagesPerPage = 10;
        $scope.srchCurrentBunch = 1;

        $scope.getAllEmployees = function(){
            AppService.getAllEmployeesCount().then(function(promise){
                $scope.employeesCount = parseInt(promise.data);
                $scope.totalPagesInt = Math.ceil($scope.employeesCount / $scope.pageSize);
                $scope.pagesCapacity = Math.floor($scope.totalPagesInt / $scope.pagesPerPage);
                $scope.tail = $scope.totalPagesInt % $scope.pagesPerPage;

                    if($scope.pagesCapacity < $scope.currentBunch) {
                        for(var i = ($scope.currentBunch * $scope.pagesPerPage) - $scope.pagesPerPage + 1;
                            i <= ($scope.currentBunch * $scope.pagesPerPage) - $scope.pagesPerPage + $scope.tail; i++){
                            $scope.totalPages.push(i);
                        }
                    } else {
                        for(i = ($scope.currentBunch * $scope.pagesPerPage) - $scope.pagesPerPage + 1; i <= $scope.currentBunch * $scope.pagesPerPage; i++){
                            $scope.totalPages.push(i);
                        }
                    }

                    AppService.getAllEmployees(1, $scope.pageSize).then(function(promise){
                        $scope.employees.list = promise.data;
                    });
            });

        };

        $scope.nextBunchPages = function() {
            $scope.totalPages = [];
            $scope.srchTotalPages = [];
            if ($scope.searchName == null) {
                $scope.currentBunch +=1;
                $scope.getAllEmployees();
            } else {
                $scope.srchCurrentBunch +=1;
                $scope.searchEmployeeByName();
            }
        };

        $scope.prevBunchPages = function() {
            $scope.totalPages = [];
            $scope.srchTotalPages = [];
            if ($scope.searchName == null) {
                $scope.currentBunch -=1;
                $scope.getAllEmployees();
            } else {
                $scope.srchCurrentBunch -=1;
                $scope.searchEmployeeByName();
            }
        };

        $scope.clickPage = function(page) {
            if ($scope.searchName == "") {
                AppService.getAllEmployees(page, $scope.pageSize).then(function(promise){
                    $scope.employees.list = promise.data;
                });
            } else {
                AppService.searchEmployeeByName($scope.searchName, page, $scope.pageSize).then(function(promise){
                    $scope.employees.list = promise.data;
                });
            }
        };

        $scope.departments = [];
        $scope.getAllDepartments = function(){
            AppService.getAllDepartments().then(function(promise){
                $scope.departments.list = promise.data;
            });
        };

        $scope.employee = {
            id: null,
            nm: null,
            acv: false,
            dp: null
        };
        $scope.saveEmployee = function() {
            AppService.saveEmployee($scope.employee).then(function(promise){});
        };

        $scope.resetEmployee = function() {
            $scope.employee = {
                id: null,
                nm: null,
                acv: false,
                dp: null
            };
        };

        $scope.editEmployee = function(value) {
            $scope.employee = value;
        };

        $scope.deleteEmployee = function(value) {
            AppService.deleteEmployee(value).then(function(promise){});
        };

        $scope.searchName = null;
        $scope.searchEmployeeByName = function() {
            AppService.searchEmployeeByNameCount($scope.searchName).then(function(promise){
                $scope.employeesCount = parseInt(promise.data);
                $scope.srchTotalPagesInt = Math.ceil($scope.employeesCount / $scope.srchPageSize);
                $scope.srchPagesCapacity = Math.floor($scope.srchTotalPagesInt / $scope.srchPagesPerPage);
                $scope.tail = $scope.srchTotalPagesInt % $scope.srchPagesPerPage;

                $scope.srchTotalPages = [];

                if($scope.srchPagesCapacity < $scope.srchCurrentBunch) {
                    for(var i = ($scope.srchCurrentBunch * $scope.srchPagesPerPage) - $scope.srchPagesPerPage + 1;
                        i <= ($scope.srchCurrentBunch * $scope.srchPagesPerPage) - $scope.srchPagesPerPage + $scope.tail; i++){
                        $scope.srchTotalPages.push(i);
                    }
                } else {
                    for(i = ($scope.srchCurrentBunch * $scope.srchPagesPerPage) - $scope.srchPagesPerPage + 1; i <= $scope.srchCurrentBunch * $scope.srchPagesPerPage; i++){
                        $scope.srchTotalPages.push(i);
                    }
                }

                AppService.searchEmployeeByName($scope.searchName, 1, $scope.pageSize).then(function(promise){
                    $scope.employees.list = promise.data;
                });
            });
        };
    }
]);
