app.factory('AppService', ['$http', function ($http) {

    return {

        getAllDepartments: function () {
            return $http.get('api/department/get/all');
        },
        saveEmployee: function (value) {
            return $http.post('api/employee/add', value);
        },
        deleteEmployee: function (value) {
            return $http.post('api/employee/delete', value);
        },
        getAllEmployeesCount: function () {
            return $http.get('api/employee/get/all/count');
        },
        getAllEmployees: function (currentPage, pageSize) {
            return $http.get('api/employee/get/all/' + currentPage + '-' + pageSize);
        },
        searchEmployeeByNameCount: function (value) {
            return $http.get('api/employee/get/by/name/' + value + '/count');
        },
        searchEmployeeByName: function (value, currentPage, pageSize) {
            return $http.get('api/employee/get/by/name/' + value + '/' + currentPage + '-' + pageSize);
        }
    };
}]);
