IF OBJECT_ID ('tblEmployees', 'U') IS NOT NULL
  DROP TABLE tblEmployees;
IF OBJECT_ID ('tblDepartments', 'U') IS NOT NULL
  DROP TABLE tblDepartments;
IF EXISTS (SELECT name FROM sys.indexes WHERE name = N'nci_tblEmployee_empName')
  DROP INDEX nci_tblEmployee_empName ON tblEmployees ;

CREATE TABLE tblDepartments (
  dpID   BIGINT PRIMARY KEY IDENTITY (1000, 1),
  dpName VARCHAR(50) NOT NULL
);

CREATE TABLE tblEmployees (
  empID     BIGINT PRIMARY KEY IDENTITY (1000, 1),
  empName   VARCHAR(50) NOT NULL,
  empActive BIT NOT NULL,
  emp_dpID  BIGINT      NOT NULL REFERENCES tblDepartments (dpID) ON DELETE CASCADE
);

INSERT INTO tblDepartments (dpName) VALUES('HR Department');
INSERT INTO tblDepartments (dpName) VALUES('Accountant Department');
INSERT INTO tblDepartments (dpName) VALUES('Engineering Department');

INSERT INTO tblEmployees (empName, empActive, emp_dpID) VALUES ('Mike', 1, 1000);
INSERT INTO tblEmployees (empName, empActive, emp_dpID) VALUES ('George', 0, 1001);
INSERT INTO tblEmployees (empName, empActive, emp_dpID) VALUES ('Anna', 0, 1002);
INSERT INTO tblEmployees (empName, empActive, emp_dpID) VALUES ('Kate', 1, 1001);
INSERT INTO tblEmployees (empName, empActive, emp_dpID) VALUES ('Andreas', 1, 1001);
INSERT INTO tblEmployees (empName, empActive, emp_dpID) VALUES ('Serge', 1, 1002);
INSERT INTO tblEmployees (empName, empActive, emp_dpID) VALUES ('Ahmad', 1, 1002);
INSERT INTO tblEmployees (empName, empActive, emp_dpID) VALUES ('Anna', 1, 1000);
INSERT INTO tblEmployees (empName, empActive, emp_dpID) VALUES ('Janno', 1, 1001);
INSERT INTO tblEmployees (empName, empActive, emp_dpID) VALUES ('Lily', 1, 1002);
INSERT INTO tblEmployees (empName, empActive, emp_dpID) VALUES ('Danna', 1, 1002);
INSERT INTO tblEmployees (empName, empActive, emp_dpID) VALUES ('Matilda', 1, 1000);
INSERT INTO tblEmployees (empName, empActive, emp_dpID) VALUES ('Veronica', 1, 1000);
INSERT INTO tblEmployees (empName, empActive, emp_dpID) VALUES ('Andrey', 1, 1000);
INSERT INTO tblEmployees (empName, empActive, emp_dpID) VALUES ('Imtiaz', 1, 1001);

DECLARE @i INT = 1
WHILE( @i <= 10)
  BEGIN
    INSERT INTO tblEmployees (empName, empActive, emp_dpID) SELECT empName, empActive, emp_dpID FROM tblEmployees
    SET @i = @i + 1
  END;

CREATE NONCLUSTERED INDEX nci_tblEmployee_empName ON tblEmployees (empName);