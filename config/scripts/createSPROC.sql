IF OBJECT_ID ('SelectAllEmployeesPngCount', 'P') IS NOT NULL
  DROP PROCEDURE SelectAllEmployeesPngCount;
IF OBJECT_ID ('SelectAllEmployeesPng', 'P') IS NOT NULL
  DROP PROCEDURE SelectAllEmployeesPng;

IF OBJECT_ID ('SelectByLikeNameEmployeesPngCount', 'P') IS NOT NULL
  DROP PROCEDURE SelectByLikeNameEmployeesPngCount;
IF OBJECT_ID ('SelectByLikeNameEmployeesPng', 'P') IS NOT NULL
  DROP PROCEDURE SelectByLikeNameEmployeesPng;

CREATE PROCEDURE SelectAllEmployeesPngCount
AS
  BEGIN
    SELECT COUNT(1) FROM tblEmployees INNER JOIN tblDepartments ON emp_dpID = dpID
  END;

CREATE PROCEDURE SelectAllEmployeesPng
    @PageNumber INT,
    @PageSize INT
AS
  BEGIN
    SELECT empID, empName, empActive, emp_dpID, dpName FROM tblEmployees INNER JOIN tblDepartments ON emp_dpID = dpID
    ORDER BY empID OFFSET @PageSize * (@PageNumber - 1) ROWS
    FETCH NEXT @PageSize ROWS ONLY
  END;

CREATE PROCEDURE SelectByLikeNameEmployeesPngCount
  @SearchName VARCHAR(50)
AS
  BEGIN
    SELECT COUNT(1) FROM tblEmployees INNER JOIN tblDepartments ON emp_dpID =  dpID WHERE empName LIKE @SearchName
  END;

CREATE PROCEDURE SelectByLikeNameEmployeesPng
    @SearchName VARCHAR(50),
    @PageNumber INT,
    @PageSize INT
AS
  BEGIN
    SELECT empID, empName, empActive, emp_dpID, dpName FROM tblEmployees INNER JOIN tblDepartments ON emp_dpID =  dpID WHERE empName LIKE @SearchName
    ORDER BY empID OFFSET @PageSize * (@PageNumber - 1) ROWS
    FETCH NEXT @PageSize ROWS ONLY
  END;
